package com.example.uidemo;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import static android.content.Context.MODE_PRIVATE;

// This file shows a simple setup to communicate with the Seamly chatbot
// To demonstrate working navigation, the Android tutorial 'Fragments' is used as a basis
// The relevant portion of the code is highlighted in comments below.

public class SecondFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Connect to your hosted web page here
        String url = "https://developers.seamly.ai/clients/web-ui/demos/app/index.html";
        WebView webView = (WebView) view.findViewById(R.id.webview);
        webView.loadUrl(url);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(this, "SeamlyBridge");

        // This demo app uses SharedPreferences as a simple data storage mechanism.
        // You can connect to your own preferred data storage here.
        SharedPreferences sharedPreferences = this.getActivity().getPreferences(MODE_PRIVATE);

        // Communicate the stored data to JavaScript on initialisation.
        // When changing data storage, it would be easiest to keep these lines
        // and substitute your own variable.
        // If you want to make other changes here, for instance to combine JavaScript,
        // be sure to keep the naming ('window.seamlyBridgeData') and data format intact.
        String result = sharedPreferences.getString("seamly", "");
        if(!result.isEmpty()) {
            webView.evaluateJavascript("window.seamlyBridgeData=" + result, null);
        }
    }

    @JavascriptInterface
    public void setData(String data) {
        // The data storage part of the bridge.
        // Optionally change these lines when using your own data storage.
        // Make sure to store the received data as-is.
        SharedPreferences sharedPreferences = this.getActivity().getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("seamly", data);
        editor.commit();
    }

    @JavascriptInterface
    public String getData() {
        // The data retrieval part of the bridge.
        // Optionally change these lines when using your own data storage.
        SharedPreferences sharedPreferences = this.getActivity().getPreferences(MODE_PRIVATE);
        String result = sharedPreferences.getString("seamly", "");
        if(!result.isEmpty()) {
            return result;
        }
        // Note that the Android JavaScript bridge uses JSON parsing.
        // This does not accept empty strings or falsy variables.
        return "{}";
    }
}